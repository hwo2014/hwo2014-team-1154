from __future__ import division
import json
import socket
import sys
import itertools
import logging
import datetime
import pdb
import numpy as np
from collections import deque
from functools import partial


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.throttle(0.5)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

class TestBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.last_position = None
        self.angles = deque(maxlen=20)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))
        # Log what the bot sends to the server
        logging.debug('Sent: ' + json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        # Setup logging , join a race, start the message loop
        filename = 'TestBot' + str(datetime.datetime.now()) + '.log'
        logging.basicConfig(filename=filename,format='%(asctime)s %(message)s',level=logging.DEBUG)
        print('Logging to ' + filename)
        self.join()
        self.msg_loop()

    def on_your_car(self, data):
        # Store the name of the bot's car
        self.name = data['name']

    def on_game_init(self, data):
        # Store the track
        self.track = data['race']['track']

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        # Out of the positions of all cars, find the bot
        me = filter(self.is_me, data)[0]
        self.angles.append(me['angle'])
        if self.last_position is not None:
            # Log current speed
            logging.debug('Current speed=' + str(self.distance_traveled(me['piecePosition'])))
            logging.debug('Current slip-angle=' + str(self.slip_angle(me['piecePosition'])))
        self.throttle(0.7)
        # Store our position for speed calculation
        self.last_position = me['piecePosition']

        # if me['piecePosition']['pieceIndex'] == 14 and me['piecePosition']['inPieceDistance'] > 16:
        #     pdb.set_trace()

        # if len(self.angles) < 5:
        #     self.throttle(1)
        # else:
        #     x = range(len(self.angles))
        #     p = np.polyfit(x,self.angles,4)
        #     pp = np.polyder(p)
        #     predicted_deriv = abs(np.polyval(pp,x[-1]+1))
        #     logging.debug('predicted_deriv='+str(predicted_deriv))
        #     self.throttle(int(1/np.exp(3*predicted_deriv) * 100) / 100)

        # curr_piece_ix = me['piecePosition']['pieceIndex']
        # if 'length' in self.pieces[curr_piece_ix+1].keys():
        #     self.throttle(1)
        # else:
        #     self.throttle(0.5)

    def slip_angle(self, curr_position):
        # -arctan(v_y / abs(v_x))
        # v_y = v * sin theta
        # v_x = v * cos theta
        v = self.distance_traveled(curr_position)
        if 'angle' in self.track['pieces'][curr_position['pieceIndex']]:
            theta = self.track['pieces'][curr_position['pieceIndex']]['angle']
            pdb.set_trace()
            return - np.arctan((v * np.sin(theta)) / abs(v * np.cos(theta)))
        else:
            return 0


    def is_me(self, piece):
        '''Returns whether the given piece represents this bot.'''
        return self.name == piece['id']['name']

    def arc_length(self, bend_piece, lane):
        '''Returns an approximation of the arc length of the given lane on the given bend piece'''
        # S = 2*pi*r*theta / 360
        C = 2*np.pi*(bend_piece['radius'] - np.sign(bend_piece['angle'])*self.distance_from_center(lane))
        return C*abs(bend_piece['angle'])/360

    def distance_from_center(self, lane):
        '''Given a lane object, return the average distance from center of a piece'''
        # The bot is usually in one lane, but if it's in the process of switching it will be in two
        lanes = filter(partial(self.in_lane, lane), self.track['lanes'])
        # If the bot is switching lanes, return the average distance from center of the two
        return reduce(lambda x, lane: x + lane['distanceFromCenter'], lanes, 0) / len(lanes)

    def in_lane(self, curr_lane, poss_lane):
        '''Returns whether the given lane positions of the bot are equal to the given possible lane'''
        # The bot is usually in one lane, but in switching will be in two
        return curr_lane['startLaneIndex'] == poss_lane['index'] or curr_lane['endLaneIndex'] == poss_lane['index']

    def distance_traveled(self, curr_position):
        # If the car is still on the same piece, calculating distance traveled is easy
        if curr_position['pieceIndex'] == self.last_position['pieceIndex']:
            return curr_position['inPieceDistance'] - self.last_position['inPieceDistance']
        else:
            # If it's a straigaway piece, calcilating distance traveled is easy
            if 'length' in self.track['pieces'][self.last_position['pieceIndex']]:
                return self.track['pieces'][self.last_position['pieceIndex']]['length'] - self.last_position['inPieceDistance'] + curr_position['inPieceDistance']
            # The case where we're leaving a bend piece
            else:
                return self.arc_length(self.track['pieces'][self.last_position['pieceIndex']], curr_position['lane']) - self.last_position['inPieceDistance'] + curr_position['inPieceDistance']

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            logging.debug('Received: ' + json.dumps(msg))
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = TestBot(s, name, key)
        bot.run()
